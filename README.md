# GOSH Materials

This repository contains design files for various GOSH Community materials such as stickers, banners, and flyers. 

## Table of Contents

### Stickers
- [PNG file](https://gitlab.com/gosh-community/gosh-materials/-/blob/main/GOSH-2023-Stickers-03.png)
- [SVG file](https://gitlab.com/gosh-community/gosh-materials/-/blob/main/GOSH-2023-Stickers-03.svg)
- [AI file](https://gitlab.com/gosh-community/gosh-materials/-/blob/main/GOSH-2023-Stickers-03__1_.ai)

### Banner

- Banner #1 [SVG file](https://gitlab.com/gosh-community/gosh-materials/-/blob/main/GOSH-2023-Banner-03.svg)
- Banner #1 [PDF file](https://gitlab.com/gosh-community/gosh-materials/-/blob/main/GOSH-2023-Banner-03__1_.pdf)
- Banner #2 [SVG file](https://gitlab.com/gosh-community/gosh-materials/-/blob/main/GOSH-2023-Banner-04.svg)
- Banner #2 [PDF file](https://gitlab.com/gosh-community/gosh-materials/-/blob/main/GOSH-2023-Stickers-03.png)

To download images, please visit the [GOSH Community Flickr](https://www.flickr.com/photos/goshcommunity)

### Flyers
- Flyers [SVG files](https://gitlab.com/gosh-community/gosh-materials/-/blob/main/GOSH_flyers.zip)